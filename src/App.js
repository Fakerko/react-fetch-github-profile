import React, { Component } from 'react';
import './scss/App.scss';
import FetchProfile from './components/FetchProfile';

class App extends Component {
	render() {
		return (
			<div className="App">
				<header className="App-content">
					<FetchProfile></FetchProfile>
				</header>
			</div>
		);
	}
}

export default App;
