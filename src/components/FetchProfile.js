import React from 'react';

const GITHUB_API_URL = "https://api.github.com/";

class FetchProfile extends React.Component {

	state = {
		userData: [],
		likes: 0
	}

	fetchFromAPI = (e) => {
		e.preventDefault();
		const USERNAME = this.refs.username.value;
		fetch(GITHUB_API_URL + "users/" + USERNAME).then(response => response.json()).then((result) => {
			this.setState({
				userData: result,
				likes: 0
			});
		});
	}

	handleLike = (e) => {
		e.preventDefault();

		this.setState({
			likes: this.state.likes + 1
		});
	}

	render() {
		const profile = this.state.userData;
		const likes = this.state.likes;
		return (
			<React.Fragment>
				<form>
					<input name="username" id="username" placeholder="Fill Username" ref="username" type="text" />
					<button onClick={this.fetchFromAPI.bind(this)}>Fetch profile</button>
				</form>
				
				{profile.length !== 0 &&
					<div className="profile">
						<div className="profile-image">
							<img src={profile.avatar_url} alt={profile.login} />
						</div>
						<div className="profile-info">
							<h1>{profile.login}</h1>
							{profile.bio &&
								<p>{profile.bio}</p>
							}
							<button onClick={this.handleLike.bind(this)}>Like</button>
							<span className="ml-1">{likes}</span>
						</div>	
					</div>
				}
			</React.Fragment>
		);
	}
}

export default FetchProfile;